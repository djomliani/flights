import Vue from "vue";
import { getAircrafts } from "../api";

export default {
  state: {
    items: [],
    pending: false,
    error: "",
    lastDv: ""
  },
  getters: {
    getAircraftsSorted: state => (sortBy, isReversed) => {
      return state.items.sort((a, b) => {
        if (a[sortBy] > b[sortBy]) {
          return isReversed ? 1 : -1;
        }
        if (a[sortBy] < b[sortBy]) {
          return isReversed ? -1 : 1;
        }
        return 0;
      });
    },
    getAircraftById: state => id => {
      return state.items.find(item => item.Id == id);
    }
  },
  mutations: {
    GET_AIRCRAFTS_PENDING(state) {
      Vue.set(state, "pending", true);
      Vue.set(state, "error", "");
    },
    GET_AIRCRAFTS_SUCCESS(state, location) {
      Vue.set(state, "items", location);
      Vue.set(state, "pending", false);
    },
    GET_AIRCRAFTS_ERROR(state, error) {
      Vue.set(state, "error", error);
      Vue.set(state, "pending", false);
    },
    SET_LAST_DV(state, lastDv) {
      state.lastDv = lastDv;
    }
  },
  actions: {
    fetchAircrafts({ commit, state }, coords) {
      commit("GET_AIRCRAFTS_PENDING");

      if (!coords || !coords.latitude || !coords.longitude) {
        commit("GET_AIRCRAFTS_ERROR", "Geolocation data is missing!");
      }

      getAircrafts(coords.latitude, coords.longitude, state.lastDv)
        .then(response => {
          commit("GET_AIRCRAFTS_SUCCESS", response.data.acList);
          commit("SET_LAST_DV", response.data.lastDv);
        })
        .catch(err => {
          commit("GET_AIRCRAFTS_ERROR", err.message);
        });
    }
  }
};
