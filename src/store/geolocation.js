import Vue from "vue";

export default {
  state: {
    coords: undefined,
    pending: false,
    error: undefined
  },
  mutations: {
    GET_GEOLOC_PENDING(state) {
      Vue.set(state, "pending", false);
    },
    GET_GEOLOC_SUCCESS(state, coords) {
      state.coords = coords;
      Vue.set(state, "pending", false);
    },
    GET_GEOLOC_ERROR(state, error) {
      Vue.set(state, "error", error);
      Vue.set(state, "pending", false);
    }
  },
  actions: {
    fetchGeolocation({ commit, state }) {
      return new Promise((resolve, reject) => {
        if (state.coords) {
          resolve();
          return;
        }

        commit("GET_GEOLOC_PENDING");

        if (!navigator.geolocation) {
          commit(
            "GET_GEOLOC_ERROR",
            "Your browser does not support Geolocation API."
          );
          reject();
          return;
        }

        navigator.geolocation.getCurrentPosition(
          position => {
            commit("GET_GEOLOC_SUCCESS", position.coords);
            resolve(position.coords);
          },
          () => {
            commit(
              "GET_GEOLOC_ERROR",
              "You need to allow Geolocation API so it can access to your location."
            );
            reject();
          }
        );
      });
    }
  }
};
