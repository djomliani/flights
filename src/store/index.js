import Vue from "vue";
import Vuex from "vuex";
import geolocation from "./geolocation";
import aircraft from "./aircraft";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    aircraft,
    geolocation
  }
});
