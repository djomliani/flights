import axios from "axios";

const proxyUrl = "https://cors-anywhere.herokuapp.com/";

export const getAircrafts = (lat, lng, lastDv) => {
  return axios.get(
    proxyUrl +
      "https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json",
    {
      params: {
        trFmt: "s",
        refreshTrails: 1,
        ldv: lastDv,
        lat: lat,
        lng: lng,
        fDstL: 0,
        fDstU: 300
      }
    }
  );
};

export const getCompanyLogo = companyName => {
  return axios.get(
    proxyUrl + "https://autocomplete.clearbit.com/v1/companies/suggest",
    {
      params: {
        query: companyName
      }
    }
  );
};
