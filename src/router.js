import Vue from "vue";
import Router from "vue-router";
import PageHome from "./pages/PageHome";
import PageAircraft from "./pages/PageAircraft";
import PageError from "./pages/PageError";
import store from "@/store";

Vue.use(Router);

const geolocationRouteMiddleware = next => {
  store
    .dispatch("fetchGeolocation")
    .then(() => {
      next();
    })
    .catch(() => {
      // redirect to error page
      next("error");
    });
};

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: PageHome,
      beforeEnter: (to, from, next) => {
        geolocationRouteMiddleware(next);
      }
    },
    {
      path: "/aircrafts/:id",
      name: "aircraft",
      component: PageAircraft,
      beforeEnter: (to, from, next) => {
        geolocationRouteMiddleware(next);
      }
    },
    {
      path: "/error",
      name: "error",
      component: PageError
    }
  ]
});
